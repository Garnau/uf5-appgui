/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assegurança;

/**
 *
 * @author usuari
 */
public class AlcadaInsuficientException extends AlcadaException {
       
    public AlcadaInsuficientException() {
        super();
    }
    
    public AlcadaInsuficientException(String msg){
      super(msg);
    }
}