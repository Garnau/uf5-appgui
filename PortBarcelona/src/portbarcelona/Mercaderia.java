/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portbarcelona;

/**
 *
 * @author usuari
 */
public class Mercaderia {

    private String mercaderia = "";
    private double volum = 0;
    private String desc = "";

    public Mercaderia(String mercaderia, double volum, String desc) {
        this.mercaderia = mercaderia;
        this.volum = volum;
        this.desc = desc;

    }

    //<editor-fold defaultstate="collapsed" desc="GETTER SETTER">
    public String getMercaderia() {
        return mercaderia;
    }

    public void setMercaderia(String mercaderia) {
        this.mercaderia = mercaderia;
    }

    public double getVolum() {
        return volum;
    }

    public void setVolum(double volum) {
        this.volum = volum;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    //</editor-fold>

}
