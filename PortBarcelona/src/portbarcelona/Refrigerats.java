/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portbarcelona;


/**
 *
 * @author usuari
 */
public class Refrigerats extends Contenidor {
    
    private double temp;
       
    public Refrigerats(String nSerie, double capacitat, int qMercaderies, String estat, String tipus) {
        super(nSerie, capacitat, qMercaderies, estat, "Refrigerats");
        
        setCapacitat(capacitat);
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTER SETTER">
    public double getCapacitatDouble(){
        return temp;
    }

    public void setCapacitat(double capacitat){
        this.temp = capacitat + ((0.01 * capacitat) / 100);
    }
//</editor-fold>
    
}
