/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assegurança;

/**
 *
 * @author usuari
 */
public class PortaEntrada {
    
    public static final int MAX_ALCADA = 190;
    public static final int MIN_ALCADA = 150;
    public static int alcada = 0;
    
    public static void comprovar(double alcada) throws AlcadaException {
        
        if(alcada < MIN_ALCADA)
            throw new AlcadaInsuficientException("no arriba a l'alçada mínima de " + MIN_ALCADA);
        
        if(alcada > MAX_ALCADA) 
            throw new SobrepassatAlcadaException("alçada màxima de " + MAX_ALCADA + " sobrepassada");
        
        if(alcada >= MIN_ALCADA && alcada <= MAX_ALCADA) 
            System.out.println("Obrint la porta a l'alçada " + alcada + " cms");

    }
    
}
