/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portbarcelona;

/**
 *
 * @author usuari
 */
class TestContenidors {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Vaixell vaixell = new Vaixell(1);

        DryVan contenidor1 = new DryVan("1", 8, 8, "obert", "negre", "DryVan");

        Cisterna contenidor2 = new Cisterna("78", 235, 235, "obert", "potatinator");

        Refrigerats contenidor3 = new Refrigerats("235", 25, 2352, "obert", "Refrigerats");

        contenidor1.afegirMercaderies(new Mercaderia("patata", 234, "patatat"));

        contenidor2.afegirMercaderies(new Mercaderia("patata", 234, "patatat"));

        contenidor3.afegirMercaderies( new Mercaderia("patata", 234, "patatat"));

        vaixell.carregarContenidor(vaixell, contenidor1);

        vaixell.carregarContenidor(vaixell, contenidor2);

        vaixell.carregarContenidor(vaixell, contenidor3);

        vaixell.mostrarContenidors(vaixell);

        vaixell.descarregarVaixell(vaixell);

    }

}
