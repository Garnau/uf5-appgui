/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portbarcelona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 *
 * @author usuari
 */
public class Vaixell {

    public static final int MAX_CONT = 1000;

    private int nVaixell = 0;
    private int qContenidors = 0;
    private Contenidor[] contenidors;

    public Vaixell(int nVaixell) {

    }

    public void carregarContenidor(Vaixell vaixell, Contenidor contenidor) {

        if (vaixell.qContenidors < 1000) {

            if (contenidor.getEstat().equals("tancat") && contenidor.getMercaderies() != null) {

                if (contenidor.getColor().equals("negre") || contenidor.getTemp() > -10) {
                        System.out.println("El contenidor és de color negre, no es pot carregar.");
                } else {
                    this.contenidors[0] = contenidor;
                    this.qContenidors++;
                }

            } else {

                System.out.println("El contenidor no conté mercaderies o no està plè.");
            }

        } else {
            System.out.println("No es poden afegir més de 1000 contenidors");
        }

    }

    public void mostrarContenidors(Vaixell vaixell) {

        for (int i = 0; i < vaixell.qContenidors; i++) {
            System.out.println(vaixell.contenidors[i].getnSerie());
        }
    }

    public void descarregarVaixell(Vaixell vaixell) {

        double volum = 0;
        double volumTotal = 0;
        String data = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        System.out.println("PORT DE BARCELONA");
        System.out.println("DATA " + data);
        System.out.println("CONTENIDOR              VOLUM");
        System.out.println("-----------------------------");

        for (int i = 0; i < this.qContenidors; i++) {

            if (vaixell.contenidors[i].getTipus().equals("Cisterna")) {
                volum = vaixell.contenidors[i].getCapacitat() / 1000;
                volumTotal = volumTotal + volum;

            } else {
                volum = vaixell.contenidors[i].getCapacitat();
                volumTotal = volumTotal + volum;
            }

            System.out.println(vaixell.contenidors[i].getnSerie() + "           " + volum + " m3");
        }

        System.out.println("VOLUM TOTAL:            " + volumTotal + " m3");

        Arrays.fill(contenidors, null);

    }

    //<editor-fold defaultstate="collapsed" desc="GETTER SETTER">
    public int getnVaixell() {
        return nVaixell;
    }

    public void setnVaixell(int nVaixell) {
        this.nVaixell = nVaixell;
    }

    public int getqContenidors() {
        return qContenidors;
    }

    public void setqContenidors(int qContenidors) {
        this.qContenidors = qContenidors;
    }

    public Contenidor[] getContenidors() {
        return contenidors;
    }

    public void setContenidors(Contenidor[] contenidors) {
        this.contenidors = contenidors;
    }

    //</editor-fold>  
}
